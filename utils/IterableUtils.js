const simpleTypes = ['string', 'number', 'boolean', 'char'];

const IterableUtils = {
    merge: (array1, array2) => {
        if (!array1) {
            array1 = [];
        }
        if (!array2) {
            return array1;
        }
        for (let i = 0; i < array2.length; i++) {
            array1.push(array2[i]);
        }
        return array1;
    },
    mergeDeep: (array1, array2) => {
        if (!array1) {
            array1 = [];
        }
        if (!array2) {
            return array1;
        }
        for (let i = 0; i < array2.length; i++) {
            const val = array2[i];
            if (val === null || val === undefined || simpleTypes.indexOf(typeof val) > -1) {
                array1.push(val);
            } else {
                array1.push(IterableUtils.extend(true, {}, val));
            }
        }
        return array1;
    },
    extend: function () {
        const args = arguments;
        const deep = args[0] === true;
        const index = deep ? 1 : 0;
        const target = arguments[index] || {};
        for (let i = index + 1; i < args.length; i++) {
            for (let key in args[i]) {
                if (args[i].hasOwnProperty(key)) {
                    const arg = args[i][key];
                    if (deep) {
                        if (arg === null || arg === undefined || simpleTypes.indexOf(typeof arg) > -1) {
                            target[key] = arg;
                        } else if (Array.isArray(arg)) {
                            target[key] = IterableUtils.mergeDeep([], arg);
                        } else {
                            target[key] = IterableUtils.extend(true, {}, arg);
                        }
                    } else {
                        target[key] = arg;
                    }
                }
            }
        }
        return target;
    }
};
export default IterableUtils;
export const merge = IterableUtils.merge;
export const extend = IterableUtils.extend;