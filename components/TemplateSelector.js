import React, {Component} from 'react';
import {Radio} from 'react-form';
import TemplateRow from './TemplateRow.js';

export default class TemplateSelector extends Component {

    onTemplateSelected(id){
        this.setState({selectedId: id}, this.props.callback(id));
    }

    getId(){
        return this.props.id ? ("template_selector_" + Math.floor((Math.random() * 1000) + 1)) : this.props.id;
    }

    render () {
        let templateList = this.props.templateList || [];
        let rows = [];
        templateList.forEach((item, index) => {
            rows.push(<TemplateRow key={index} rowData={item} isChecked={item.id === this.props.selectedId} onTemplateSelected={(id) => this.onTemplateSelected(id)}/>);
        });
        return <div id={this.getId()} className="template-selector">
            {rows}
        </div>
    }
}