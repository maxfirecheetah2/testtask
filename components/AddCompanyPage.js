import React, {Component} from 'react';
import { Form, Text, StyledText, Select, Checkbox, RadioGroup, Radio } from 'react-form';
import TemplateSelector from './TemplateSelector.js';
import DayPickerInput from 'react-day-picker/DayPickerInput';

const methodOptions = [
    {
        label: 'Method1',
        value: 'method1'
    },
    {
        label: 'Method2',
        value: 'method2'
    },
    {
        label: "Method3",
        value: 'method3'
    }
];

const templateList = [
    {id: 1, templateName: 'Template1', filename: null, lastUpdated: '19/12/2016'},
    {id: 2, templateName: 'Template2', filename: null, lastUpdated: '18/11/2017'},
    {id: 3, templateName: 'Template3', filename: null, lastUpdated: '08/02/2017'}
];


export default class AddCompanyPage extends Component {

    static TITLE = "Neue Kundenlebenszyklus-Kampagne";

    constructor(props){
        super(props);
        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            selectedTemplateId: 1,
            startDateError: null
        };
    }

    onItemSelected(value){
        this.setState({selectedTemplateId: value});
    }

    onSubmit(data){
        data.startDate = this.state.startDate
        data.endDate = this.state.endDate;
        data.selectedTemplateId = this.state.selectedTemplateId;
        console.log("DATA", data);
        alert("Successfully sent!");
    }

    onSetStartDate(date){
        this.setState({startDate: date});
    }

    onSetEndDate(date){
        this.setState({endDate: date});
    }

    errorValidator = ( values ) => {
        const validateCompanyName = ( companyName ) => {
            return !companyName ? 'Campaign name is required!' : null;
        };
        const integerValidator = (number) => {
            return !hour || isNaN(number) || number === ''  ? "Enter a whole number!" : null;
        };
        return {
            companyName: validateCompanyName(values.companyName),
            hour: integerValidator(values.hour),
            day:  integerValidator(values.day),
            month: integerValidator(values.month)
        };
    };

    render () {
        return <div className="new-company-form">
            <h3>{AddCompanyPage.TITLE}</h3>
            <Form onSubmit={submittedValues => this.onSubmit(submittedValues)}
                  validateError={this.errorValidator}>
                {formApi => (
                    <form onSubmit={formApi.submitForm} id="add_company_form">
                        <div className="row form-group">
                            <div className="col-sm-6">
                                <label htmlFor="companyName">Kampagne Name</label>
                                <StyledText field="companyName" id="companyName" className="form-control"/>
                            </div>
                        </div>
                        <div className="row form-group">
                            <div className="col-sm-6">
                                <label htmlFor="startDate">Beginn</label>
                                <DayPickerInput field="startDate" onChange={(date) => this.onSetStartDate(date)} value={this.state.startDate}/>
                            </div>
                            <div className="col-sm-6">
                                <label htmlFor="endDate">Ende</label>
                                <DayPickerInput field="endDate" onChange={(date) => this.onSetEndDate(date)} value={this.state.endDate}/>
                            </div>
                        </div>
                        <hr className="featurette-divider"/>
                        <legend>Methode</legend>
                        <div className="row form-group">
                            <div className="col-sm-6">
                                <Select field="method1" id="method1" options={methodOptions} className="form-control"/>
                            </div>
                            <div className="col-sm-6">
                                <Select field="method2" id="method2" options={methodOptions} className="form-control"/>
                            </div>
                        </div>
                        <legend>Wann auslösen?</legend>
                        <div className="row form-group">
                            <div className="col-sm-2">
                                <label htmlFor="hour">Studen</label>
                                <StyledText field="hour" id="hour" className="form-control"/>
                            </div>
                            <div className="col-sm-2">
                                <label htmlFor="day">Tage</label>
                                <StyledText field="day" id="day" className="form-control"/>
                            </div>
                            <div className="col-sm-2">
                                <label htmlFor="month">Monnate</label>
                                <StyledText field="month" id="month" className="form-control"/>
                            </div>
                        </div>
                        <hr className="featurette-divider"/>
                        <TemplateSelector id="template_selector" templateList={templateList} callback={(id) => this.onItemSelected(id)} selectedId={this.state.selectedTemplateId}/>
                        <div className="actions">
                            <input className="btn btn-dark new_template" type="button" value="Neues Template"/>
                            <button className="btn btn-primary save_company" type="submit">Kampagne Sichern</button>
                        </div>
                    </form>
                )}
            </Form>
        </div>
    }
}