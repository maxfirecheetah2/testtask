import React, {Component} from 'react';
import AddCompanyPage from './AddCompanyPage'

export default class App extends Component {

    render () {
        return <div className="app">
            <div className="back">
                <div className="div-center">
                    <div className="content">
                        <AddCompanyPage/>
                    </div>
                </div>
            </div>
        </div>
    }
}