import React, {Component} from 'react';
import {Radio} from 'react-form';

export default class TemplateRow extends Component {

    render(){
        let data = this.props.rowData;
        return <div className="template-item">
            <div className="row">
                <div className="col-sm-6">
                    <input className="template-radio" type="radio" id={"radioButton_" + data.id}
                           onClick={() => this.props.onTemplateSelected(data.id)}
                           onChange={()=>{}}
                           checked={this.props.isChecked}/>
                    <div className="template-name">{data.templateName}</div>
                    <div className="last-updated">Aktualisiert am {data.lastUpdated}</div>
                </div>
                <div className="col-sm-6">
                    <a href="#" onClick={(e) => {e.preventDefault()}}><strong>Vorschau<link rel="icon" type="image/png" href="/icons/eye-24.png"/></strong></a>
                </div>
            </div>
            <hr className="featurette-divider"/>
        </div>
    }

}